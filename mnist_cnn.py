#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
import torchvision
import torch.nn as nn
import torchvision.transforms as transforms
import cv2
import numpy as np


# In[2]:


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)


# In[3]:


# hyperparameters
num_class = 10
num_epochs = 25
batch_size = 256
learning_rate = 0.0001
trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0,), (1,))])   # normalize with ) mean and STD as 1


# In[4]:


train_dataset = torchvision.datasets.MNIST(root = '/home/himanshu/mnist_classi', train=True, transform=trans, download=True)
test_dataset = torchvision.datasets.MNIST(root = '/home/himanshu/mnist_classi', train=False, transform=trans, download=True)

#data loader
train_loader=torch.utils.data.DataLoader(dataset = train_dataset, batch_size=batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=False)


# In[12]:


image,label=train_dataset[2]
print(image.size(),'\n', label)
print(train_dataset)


# In[6]:


class ConvNet(nn.Module):
    def __init__(self,num_class):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1,16,kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2,stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16,32, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc = nn.Linear(7*7*32, num_class)
        
    def forward(self, x):
        l1=self.layer1(x)
        l2=self.layer2(l1)
        l3=l2.reshape(l2.size(0), -1)
        out=self.fc(l3)
        return out
    


# In[7]:


model = ConvNet(num_class).to(device)
#print(model)

#loss optimizer
criteria = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)


# In[8]:


#train model
total_step = len(train_loader)

for epoch in range(num_epochs):
    c = 0
    for i, (images, labels) in enumerate(train_loader):
        images=images.to(device)
        labels=labels.to(device)
        
        #forward pass
        outputs = model(images)
        loss = criteria(outputs, labels)
        
        #backprop and optimize
        optimizer.zero_grad()  #Clears the gradients of all optimized torch.Tensor
        loss.backward()
        optimizer.step()
        c += loss.item()
        #if (i+1) % 1000 == 0:
            #print ('Epoch {}, Step [{}/{}], Loss: {:.4f}'.format(epoch+1, i+1, total_step, loss.item()))
    #print(c, c/60000)
    print('Epoch {}, loss: {:.8f}'.format(epoch+1, c/60000))
        


# In[9]:


# Test the model
model.eval()  # eval mode (batchnorm uses moving mean/variance instead of mini-batch mean/variance)
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in test_loader:
        images = images.to(device)
        labels = labels.to(device)
        outputs = model(images)
        _, pred = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (pred==labels).sum().item()
    print('test accuracy: {} %'.format(100*correct/total))


# In[11]:


torch.save(model.state_dict(), 'cnn_mnist_retrained.pth')


# In[ ]:




