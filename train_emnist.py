import cv2
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import os
from PIL import Image
from PIL import ImageOps
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor
from IPython.display import clear_output


idx = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']



def pil_loader(path):
	with open(path, 'rb') as f:
		img = Image.open(f)
		return img.convert('L')

BATCH_SIZE = 8192
START_EPOCH = 1
END_EPOCH = 25
DISPLAY = 10
num_class = 26

train_set= ImageFolder(root="/home/himanshu/dl/dataset/EMNIST/by_merge/train", transform=ToTensor(), loader=pil_loader)
train_loader = DataLoader(train_set, batch_size=BATCH_SIZE, shuffle=True, num_workers=1)

test_set = ImageFolder(root="/home/himanshu/dl/dataset/EMNIST/by_merge/test", transform=ToTensor(), loader=pil_loader)
test_loader = DataLoader(test_set, batch_size=BATCH_SIZE, shuffle=False, num_workers=1)


print("Length Of Dataset:", len(train_set)+len(test_set))
print("Length Of Training Dataset:",len(train_set))
print("Length Of Validation Dataset:",len(test_set))


class Net(nn.Module):
	def __init__(self,num_class):
		super(Net, self).__init__()
		self.layer1 = nn.Sequential(
			nn.Conv2d(1,16,kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(16),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=2,stride=2))
		self.layer2 = nn.Sequential(
			nn.Conv2d(16,32, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(32),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=2, stride=2))
		self.layer3 = nn.Sequential(
			nn.Conv2d(32,64, kernel_size=3, stride=1, padding=1),
			nn.BatchNorm2d(64),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=2, stride=2))
		self.fc = nn.Linear(3*3*64, num_class)
		
	def forward(self, x):
		l1=self.layer1(x)
		l2=self.layer2(l1)
		l3 = self.layer3(l2)
		# print(l3.size())
		l3=l3.reshape(l3.size(0), -1)
		out=self.fc(l3)
		return out



model = Net(num_class)
# model.load_state_dict(torch.load('/home/himanshu/dl/EMNIST/letter_recognizer_20.pth'))
# print("Model Loaded")
model.cuda()


optimizer = optim.Adam(model.parameters(), lr=1e-4, eps=1e-4)
criteria = nn.CrossEntropyLoss()


print("------------------Training Started------------------")
for epoch in range(START_EPOCH, END_EPOCH+1):
	c = 0
	# torch.save(model.state_dict(), 'lighter_letter_recognizer_'+str(epoch)+'.pth')
	for batch_idx, (data, target) in enumerate(train_loader):
		data, target = data.cuda(), target.cuda()
		data, target = Variable(data), Variable(target)
		optimizer.zero_grad()
		output = model(data)
		loss = criteria(output, target)
		loss.backward()
		optimizer.step()
		c += loss.item()

		if batch_idx % DISPLAY == 0:
			 print('Train Epoch:\t {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
				 epoch, batch_idx * len(data), len(train_loader.dataset),
				 100. * batch_idx / len(train_loader), loss.item()))


	print("----- LOSS / EPOCH -----", (10000*c)/len(train_set))
	if epoch % 5 == 0:
		torch.save(model.state_dict(), 'lighter_letter_recognizer_'+str(epoch)+'.pth')
		print('Model Saved at epoch:', epoch)


model.eval()
with torch.no_grad():
	correct = 0
	total = 0
	for images, labels in test_loader:
		images = images.cuda()
		labels = labels.cuda()
		outputs = model(images)
		_, pred = torch.max(outputs.data, 1)
		total += labels.size(0)
		correct += (pred==labels).sum().item()
	print('test accuracy: {} %'.format(100*correct/total))