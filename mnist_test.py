def test(path):
	img_path = input("Enter path to image:")
	img = cv2.imread(img_path)

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	gray = cv2.resize(gray, (28,28))

	trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0,), (1,))])
	ip = trans(gray)
	#ip = torch.from_numpy(x)
	ip = ip.reshape(1,1,28,28)
	ip = ip.to('cuda')
	#print(ip)


	outputs_test = model(ip)
	_, pred = torch.max(outputs_test.data, 1)
	predx = pred.item()
	print("I Think it's a : ", predx)

	return predx